package com.revolut.android.autotests.test.bankaccount;

import com.revolut.android.autotests.TestWrapper;
import io.qameta.allure.Feature;
import org.testng.annotations.Test;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 29, 2017
 */

@Feature("Bank account feature")
public class BankAccountTests extends TestWrapper {
    //TODO: add assertions steps

    @Test
    public void addBeneficiaryAccount(){
        userSteps.signIn("+44", "1217104669", "1245");
        userSteps.openBankAcountMenu();
        userSteps.addMyselfBeneficiary("00078974", "115154");
    }
}
