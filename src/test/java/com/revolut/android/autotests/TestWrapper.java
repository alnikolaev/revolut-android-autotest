package com.revolut.android.autotests;

import com.revolut.android.autotest.steps.UserSteps;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.testng.annotations.*;

import java.net.MalformedURLException;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 29, 2017
 */
public class TestWrapper extends AndroidSetup {
    protected UserSteps userSteps;

    @BeforeMethod
    public void initialize() throws MalformedURLException {
        androidSetUpAppium();
        initializeSteps();
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

    private void initializeSteps(){
        userSteps = new UserSteps(driver);
    }

    @AfterMethod
    @Attachment
    public static byte[] makeScreenshot() {
        return driver.getScreenshotAs(OutputType.BYTES);
    }
}
