package com.revolut.android.autotest.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.*;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 29, 2017
 */
public class BaseScreen {
    protected By skipButton = By.id("com.revolut.revolut.test:id/header_next");

    protected WebDriver driver;
    protected Wait wait;
    protected int TIMEOUT = 10;

    public BaseScreen(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,TIMEOUT);
    }

    @Step("I click 'skip' button to skip tutorial")
    public void clickSkipButton() {
        wait.until(ExpectedConditions.elementToBeClickable(skipButton));
        driver.findElement(skipButton).click();
    }
}
