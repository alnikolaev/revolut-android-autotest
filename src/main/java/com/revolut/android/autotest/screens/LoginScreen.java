package com.revolut.android.autotest.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 29, 2017
 */
public class LoginScreen extends BaseScreen{
    private By signupTitle = By.id("com.revolut.revolut.test:id/signup_title");

    private By signupNextButton = By.id("com.revolut.revolut.test:id/signup_next");
    private By signupPhoneEdit = By.id("com.revolut.revolut.test:id/uic_edit_phone_number");
    private By signupCountryCodeEdit = By.id("com.revolut.revolut.test:id/uic_edit_country_code");
    private By instantMoneyTransferSkipButton = By.id("com.revolut.revolut.test:id/uic_header_next");


    private String pinDigitId = "com.revolut.revolut.test:id/digit";


    public LoginScreen(WebDriver driver) {
        super(driver);
    }

    @Step("I type phone ({0}){1}")
    public void typePhone(String countryCode, String phone){
        //TODO: country code should be chosen
        //wait.until(ExpectedConditions.visibilityOfElementLocated(signupCountryCodeEdit));
        //driver.findElement(signupCountryCodeEdit).sendKeys(countryCode);

        wait.until(ExpectedConditions.visibilityOfElementLocated(signupPhoneEdit));
        driver.findElement(signupPhoneEdit).sendKeys(phone);
    }

    @Step("I click 'next' button on signup screen")
    public void clickSignupNext(){
        wait.until(ExpectedConditions.elementToBeClickable(signupNextButton));
        driver.findElement(signupNextButton).click();
    }

    @Step("I enter pin-code ({0})")
    public void enterPinCode(String pin){
        if(pin.length() != 4 )
            throw new Error("Pin-code should have length 4. Check scenario.");
        try {Integer.parseInt(pin);}
        catch (NumberFormatException e){
            throw new Error("Pin should contain only digits. Check scenario.");
        }
        pin.chars().forEach(c -> {
            By digit = getPinDigitLocator(Character.getNumericValue(c));
            wait.until(ExpectedConditions.elementToBeClickable(digit));
            driver.findElement(digit).click();
        });
    }

    @Step("I press 'not now' button to skip instant money transfers feature")
    public void skipInstantMoneyTransferFeature(){
        wait.until(ExpectedConditions.elementToBeClickable(instantMoneyTransferSkipButton));
        driver.findElement(instantMoneyTransferSkipButton).click();
    }


    private By getPinDigitLocator(Integer digit){
        if(digit < 0 || digit > 9)
            throw new Error("Pin digit should be in 0-9. Check scenario.");

        return By.id(pinDigitId+digit);
    }
}
