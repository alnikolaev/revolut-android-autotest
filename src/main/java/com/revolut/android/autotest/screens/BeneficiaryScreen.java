package com.revolut.android.autotest.screens;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 30, 2017
 */
public class BeneficiaryScreen extends BaseScreen {

    private By title = By.id("com.revolut.revolut.test:id/main_title");

    private By addNewBeneficiaryButton = By.id("com.revolut.revolut.test:id/list_add_new_item_text");
    private By nextButton = By.id("com.revolut.revolut.test:id/button_next");
    private By toMyselfRadioButton = By.xpath("//android.widget.TextView[@text='To myself']");

    private By accountNumberEdit = By.id("com.revolut.revolut.test:id/server_field_0");
    private By sortCodeEdit = By.id("com.revolut.revolut.test:id/server_field_1");


    public BeneficiaryScreen(WebDriver driver) {
        super(driver);
    }

    @Step("I click on 'Add a new beneficiary' button")
    public void clickAddNewBeneficiary(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(addNewBeneficiaryButton));
        driver.findElement(addNewBeneficiaryButton).click();
    }

    @Step("I select 'To myself' option")
    public void selectToMySelf(){
        wait.until(ExpectedConditions.elementToBeClickable(toMyselfRadioButton));
        driver.findElement(toMyselfRadioButton).click();

        wait.until(ExpectedConditions.elementToBeClickable(nextButton));
        driver.findElement(nextButton).click();

        //click next again - this is simpliest way :)
        wait.until(ExpectedConditions.elementToBeClickable(nextButton));
        driver.findElement(nextButton).click();
    }

    @Step("I enter account ({0}) and sort code ({1})")
    public void enterAccountInfo(String account, String sortCode){
        wait.until(ExpectedConditions.visibilityOfElementLocated(accountNumberEdit));
        driver.findElement(accountNumberEdit).click();
        typeKeys(account);

        wait.until(ExpectedConditions.visibilityOfElementLocated(sortCodeEdit));
        driver.findElement(sortCodeEdit).click();
        typeKeys(sortCode);

        wait.until(ExpectedConditions.elementToBeClickable(nextButton));
        driver.findElement(nextButton).click();
    }

    //sendKeys lost some symbols => this is workaround
    private void typeKeys(String keys){
        keys.chars().forEach(c -> {
            int key = Character.getNumericValue(c);
            if(key <= 9) ((AndroidDriver) driver).pressKeyCode(key + 7);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
