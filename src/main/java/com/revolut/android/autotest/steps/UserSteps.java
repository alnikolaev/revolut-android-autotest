package com.revolut.android.autotest.steps;

import com.revolut.android.autotest.screens.*;
import io.appium.java_client.android.AndroidDriver;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 29, 2017
 */
public class UserSteps {
    protected LoginScreen loginScreen;
    protected MainScreen mainScreen;
    protected BeneficiaryScreen beneficiaryScreen;

    public UserSteps(AndroidDriver driver){
        loginScreen = new LoginScreen(driver);
        mainScreen = new MainScreen(driver);
        beneficiaryScreen = new BeneficiaryScreen(driver);
    }

    @Step("I sign in with phone {0}{1} and pin {2}")
    public void signIn(String countryCode, String phone, String pin){
        loginScreen.clickSkipButton();
        loginScreen.typePhone(countryCode, phone);
        loginScreen.clickSignupNext();
        loginScreen.enterPinCode(pin);
        loginScreen.skipInstantMoneyTransferFeature();
    }

    @Step("I open bank account menu")
    public void openBankAcountMenu(){
        mainScreen.clickTransferButton();
        mainScreen.clickToBankAccountMenu();
    }

    @Step("I add myslef benefiiary account {0} with sort code {1}" )
    public void addMyselfBeneficiary(String account, String sortCode){
        beneficiaryScreen.clickSkipButton();
        beneficiaryScreen.clickAddNewBeneficiary();
        beneficiaryScreen.selectToMySelf();
        beneficiaryScreen.enterAccountInfo(account, sortCode);
    }

}
