### Requirements

* `maven` is installed
* `adb` is installed
* Android phone or virtual device connected to local machine

## Execution

To execute all tests and generate report run the following:

```bash
mvn clean verify site
```

**FYI: nodejs and appium will be downloaded and executed automatically.