package com.revolut.android.autotest.screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * @author Aleksander Nikolaev <aleksander.nikolaev@truckerpath.com>
 * @since Oct 30, 2017
 */
public class MainScreen extends BaseScreen {
    private By transferButton = By.id("com.revolut.revolut.test:id/button_transfer");
    private By toBankAccountButton = By.xpath("//android.widget.TextView[@text='To bank account']");

    public MainScreen(WebDriver driver) {
        super(driver);
    }

    @Step("I click on 'transfer' button on main screen")
    public void clickTransferButton(){
        wait.until(ExpectedConditions.elementToBeClickable(transferButton));
        driver.findElement(transferButton).click();
    }

    @Step("I click on 'To bank account' menu option")
    public void clickToBankAccountMenu(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(toBankAccountButton));
        driver.findElement(toBankAccountButton).click();
    }
}
